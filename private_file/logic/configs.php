<?php

define("HOME", "home");

$page_name["home"] = "Strona Główna";
$page_name["about"] = "O nas";
$page_name["world"] = "Świat";
$page_name["rules"] = "Regulamin";
$page_name["races"] = "Rasy";
$page_name["magic"] = "Magia";
$page_name["deities"] = "Bóstwa";
$page_name["fractions"] = "Frakcje";
$page_name["plot"] = "Plot";
$page_name["text-editor"] = "Edytor Tekstu";
$page_name["userProfile"] = "Twój Profil";

$page_name["login"] = "Zaloguj";
$page_name["register"] = "Zarejestruj";

$page_name["error404"] = "Error 404";
$page_name["error403"] = "Error 403";

?>