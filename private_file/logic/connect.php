<?php
$db_host = "localhost";
$db_user = "root";
$db_password = "";
$db_name = "forumpbp";

mysqli_report(MYSQLI_REPORT_STRICT);

try
{
    $connect = new mysqli($db_host, $db_user, $db_password, $db_name);     
    if ($connect->connect_errno!=0)                                        
    {
    throw new Exception(mysqli_connect_errno());                             
    }
}
catch(Exception $exception)
{
    header("Location: /forumPBP/error/errorHost.php");
    exit();
    echo "<br>Exception: ".$exception;
}

?>