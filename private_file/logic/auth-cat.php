<?php
    require_once('connect.php');

    $permission = $_SESSION['user_permission'];

    $connect = @new mysqli($db_host, $db_user, $db_password, $db_name);     
    if ($connect->connect_errno!=0)                                         
    {
    echo "Error: ".$connect->connect_errno;                                 
    }
    else
    {
        if(isset($_GET['subcat']) || isset($_GET['plotid']))
        {
            if (isset($_GET['subcat'])) $catID = $_GET['subcat'];
            else $catID = $_GET['plotid'];
            $query = "SELECT catPermission FROM categories WHERE catID = '$catID'";
            $result = @$connect->query($query);
            $answer_catperm = $result->fetch_assoc();
            $answer_catperm = $answer_catperm['catPermission'];

            $query = "SELECT permissionWeight FROM permissionsWeight WHERE permissionType = '$permission'";
            $result = @$connect->query($query);
            $answer_user_permweight = $result->fetch_assoc();
            $answer_user_permweight = $answer_user_permweight['permissionWeight'];

            $query = "SELECT permissionWeight FROM permissionsWeight WHERE permissionType = '$answer_catperm'";
            $result = @$connect->query($query);
            $answer_site_permweight = $result->fetch_assoc();
            $answer_site_permweight = $answer_site_permweight['permissionWeight'];

            if($answer_user_permweight >= $answer_site_permweight)
            {
                $permitted_cat = true;
            }
            else $permitted_cat = false;
            
            if (!$permitted_cat)
            {
                $query = "SELECT superiorCat FROM cathierarchylist WHERE inferiorCat = '$catID'";
                $result = @$connect->query($query);
                $answer = $result->fetch_assoc();
                $superior_catID = $answer['superiorCat'];
                if(isset($_GET['plotid'])) header ("Location: index.php?page=home&subcat=".$superior_catID."&site=0");
                else    
                {
                    $query = "SELECT superiorCat FROM cathierarchylist WHERE inferiorCat = '$superior_catID'";
                    $result = @$connect->query($query);
                    $answer = $result->fetch_assoc();
                    $superior_catID = $answer['superiorCat'];
                    echo "y";
                    header ("Location: index.php?page=home&subcat=".$superior_catID."&site=0");
                }
                $result->free_result();
            }
        }
    }  
?>