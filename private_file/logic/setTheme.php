<?php
    $media_path = "media/";
    $what_theme = "light";
    if(isset($_COOKIE['theme'])) $what_theme = $_COOKIE['theme'];
    if ($what_theme == "dark") $images_path = $media_path."images/dark_mode";
    else $images_path = $media_path."/images/light_mode";
?>