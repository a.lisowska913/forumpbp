<div class="panel">
    <button id="theme-toggle-button" type="submit">
        <img id="mode-icon" title="zmień tryb" src="<?php echo $images_path;?>/modeSwitchIcon.png">
    </button>
    <script src="<?php echo $media_path?>js/toggle-theme.js"></script>
</div>