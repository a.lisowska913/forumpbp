<div class="menu-sites">
    <a class="menu-button" href="index.php?page=home">Strona&nbspgłówna</a>
    <div id="menu">
        <a class="site" href="index.php?page=rules">Regulamin</a>
        <a class="site" href="index.php?page=about">O&nbspnas</a>
        <a class="site" href="index.php?page=world">Świat</a>
        <a class="site" href="index.php?page=races">Rasy</a>
        <a class="site" href="index.php?page=magic">Magia</a>
        <a class="site" href="index.php?page=deities">Bóstwa</a>
        <a class="site" href="index.php?page=fractions">Frakcje</a>
    </div>
    <a href="javascript:void(0);" class="arrowIcon" id="arrowIcon1" onclick="myFunction()">
        <img class=arrow-icon src="<?php echo $images_path;?>/arrowMenuIcon.png">
    </a>
    <script src="<?php echo $media_path;?>/js/menu-expandable.js"></script>
</div>