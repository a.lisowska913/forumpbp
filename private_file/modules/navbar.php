<nav>
    <!--Pasek nawigacyjny jeśli użytkownik nie jest zalogowany-->            
    <?php if(($userPermission == "guest")) { ?> 
        <div id="nav-elements-left">
            <a class="navbar" href="index.php?page=login">Zaloguj</a>
            &nbsp&nbsp::&nbsp&nbsp
            <a class="navbar" href="index.php?page=register">Zarejestruj</a>
        </div>
        <div id="nav-icons-left">
            <a class="navbar" href="index.php?page=login">
                <img id="login-icon" title="zaloguj" src="<?php echo $images_path;?>/loginIcon.png">
            </a>
            <a class="navbar" href="index.php?page=register">
                <img id="register-icon" title="zarejestruj" src="<?php echo $images_path;?>/registerIcon.png">
            </a>
        </div>
    <?php } else {
        $avatar = $_SESSION['user_avatar'];
        $userName = $_SESSION['user_name'];

        if ($avatar == "emptyAvatarIcon.png") echo $images_path."/".$avatar;
    ?>

    <!--Pasek nawigacyjny jeśli użytkownik jest zalogowany dla wersji na komputer-->
        <div id="nav-elements-left">
            <a href="index.php?page=userProfile">                                                   <!-- przejście do profilu użytkownika przez avatar -->
                <div>
                <img src="<?php echo $avatar; ?>"
                    class="avatar-icon" title="profil"/>
                </div>
            </a> 
            <div>Zalogowano&nbspjako</div>
            <div><?php echo $userName; ?></div>                                        <!-- wyświetlony login użytkownika -->
            <div><a class="navbar" href="private_file/pages/action/logoutAction.php">Wyloguj</a></div>           <!-- przycisk wylogowania z konta -->
        </div> 
        <div id=nav-elements-right>
            <div class="navbar">Wątki ::</div>
            <div class="rotation-container">
                <div class="navbar-site">Aktywne</div>                                              <!-- przejście do wątków użytkownika -->
                <div class="gear">
                    <a href="index.php?page=activePlots">
                        <img src="<?php echo $images_path;?>/gear.png" class="animated-icon" />
                    </a>			
                </div>
            </div>
            <div class="navbar"> || </div>
            <div class="rotation-container">
                <div class="navbar-site">Oczekujące</div>                                           <!-- przejście do wątków użytkownika, oczekujących na kontynuację -->
                <div class="gear">
                    <a href="index.php?page=waitingPlots">
                        <img src="<?php echo $images_path;?>/gear.png" class="animated-icon" />
                    </a>		
                </div>
            </div>
            <div class="navbar"> || </div>
            <div class="rotation-container">
                <div class="navbar-site">Poszukujący</div>                                          <!-- przejście do użytkowników, poszukujących wątku -->
                <div class="gear">
                    <a href="index.php?page=searchingPlots">
                        <img src="<?php echo $images_path;?>/gear.png" class="animated-icon" />
                    </a>	
                </div>
            </div>
        </div>

        <!--Pasek nawigacyjny jeśli użytkownik jest zalogowany dla wersji mobilnej-->
        <div id="nav-icons-left">
            <a class="navbar-icon" href="">
                <img src="<?php echo $avatar; ?>" class="avatar-icon" title="profil"/>
            </a> 
            <a href="private_file/pages/action/logoutAction.php">
                <img id="logout-icon" title="wyloguj" src="<?php echo $images_path;?>/logoutIcon.png">
            </a>
            </div>
        </div>
        <div id=nav-icons-right>
            <div class="rotation-container">
                <a class="navbar-icon" href="">
                    <img src="<?php echo $images_path;?>/activeIcon.png" class="initial-icon" title="aktywne"/>
                    <div class="gear">
                        <img src="<?php echo $images_path;?>/gear.png" class="animated-icon" title="aktywne"/>		
                    </div>
                </a>
            </div>
            <div class="rotation-container">
                <a class="navbar-icon" href="">
                    <img src="<?php echo $images_path;?>/waitingIcon.png" class="initial-icon" title="oczekujące"/>
                    <div class="gear">
                        <img src="<?php echo $images_path;?>/gear.png" class="animated-icon" title="oczekujące"/>			
                    </div>
                </a>
            </div>
            <div class="rotation-container">
                <a class="navbar-icon" href="">
                    <img src="<?php echo $images_path;?>/searchingActiveIcon.png" class="initial-icon" title="poszukujący"/>
                    <div class="gear">
                        <img src="<?php echo $images_path;?>/gear.png" class="animated-icon" title="poszukujący"/>
                    </div>
                </a>
            </div>	
        </div>
    <?php }?>
</nav>