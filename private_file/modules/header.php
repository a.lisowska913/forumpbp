<?php $separator = "<img class='separator' src='".$images_path."/separator.png'></br>"; ?>

<html>
    <head>
        <title>Airth <?= ":: ".$title ?></title>
        <meta name="description" content="Forum z mechaniką rpg do wspólnego przeżywania niezwykłych przygód, w podniebnym świecie Airth!">
        <meta name="keywords" content="forum rp rpg opowiadania steampunk statki piraci średniowiecze ">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleUniwersal.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/stylePanel.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleNavbar.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleMenu.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleMain.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleGadgets.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleForms.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleTextEditor.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleCatEditor.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleProfile.css">
        <meta charset="UTF-8">
        <meta name="author" content="Agnieszka Lisowska">
        <meta name="viewport" content="width=device-width, initial-scale=0.8">
        <link rel="shortcut icon" href="<?php echo $media_path;?>images/favicon.png" type="image/x-icon"/>
        <script src="https://www.google.com/recaptcha/api.js"></script>

    </head>
<body>
    <!--Panel nawigacyjny użytkownika-->
    <?php include("panel.php"); ?>

    <!--Pasek nawigacyjny strony-->
    <?php include("navbar.php"); ?>

    <!--Nagłówek strony-->
    <header><div id="title">Airth</div><div id="subtitle">forum</div></header>

    <!--Menu strony-->
    <?php include("menu.php"); ?>
            
    <!--Zawartość strony-->
    <main>
        <div class="announcements">
            <img class="announcement-icon" src="<?php echo $images_path;?>/announcmentIcon.png"/>
            <p class="announcement"></p>
        </div>