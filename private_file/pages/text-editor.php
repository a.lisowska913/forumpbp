<div class="text-editor-container" id="text-editor">
    <div class="text-editor-toolbar" id="text-editor-element">
        <button type="button" class="te-button" data-element="bold">
            <i class="fa fa-bold"></i>
        </button>
        <button type="button" class="te-button" data-element="italic">
            <i class="fa fa-italic"></i>
        </button>
        <button type="button" class="te-button" data-element="underline">
            <i class="fa fa-underline"></i>
        </button>
        <button type="button" class="te-button" data-element="createLink">
            <i class="fa fa-link"></i>
        </button>
        <button type="button" class="te-button" data-element="unlink">
            <i class="fa fa-link-slash"></i>
        </button>
        <button type="button" class="te-button" data-element="formatBlock">
            <i class="fa fa-quote-right"></i>
        </button>
        <button type="button" class="te-button" data-element="justifyLeft">
            <i class="fa fa-align-left"></i>
        </button>
        <button type="button" class="te-button" data-element="justifyCenter">
            <i class="fa fa-align-center"></i>
        </button>
        <button type="button" class="te-button" data-element="justifyRight">
            <i class="fa fa-align-right"></i>
        </button>
        <button type="button" class="te-button" data-element="justifyFull">
            <i class="fa fa-align-justify"></i>
        </button>
        <button type="button" class="te-button" data-element="insertImage">
            <i class="fa fa-image"></i>
        </button>
        <button type="button" class="te-button" data-element="fontSize">
            <i class="fa fa-text-height"></i>
        </button>
        <button type="button" class="te-button" data-element="removeFormat">
            <i class="fa fa-remove-format"></i>
        </button>
        <button type="button" class="te-button" data-element="undo">
            <i class="fa fa-undo"></i>
        </button>
        <button type="button" class="te-button" data-element="redo">
            <i class="fa fa-redo"></i>
        </button>
    </div>
    
    <div class="text-editor-form">
        <form method="post" action="/forumPBP/private_file/pages/action/texteditorAction.php">
            <div id="text-panel">
                <input type="hidden" name="plotID" value="<?php echo $plotID; ?>">
                <label><h4>Tytuł posta</h4></label>
                    <input type="text" name="title" id="form-title" placeholder="Tytuł posta" required>
                <label><h4>Postać</h4></label>
                    <select id="active-character" name="active-character">
                        <?php
                            echo "<option selected>Aria</option><selected>";
                        ?>
                    </select>
                <label><h4>Kontynuacja</h4></label>
                    <select id="who-continue" name="who-continue">
                        <option selected>Dowolna osoba</option>
                        <option>Konkretna osoba</option>
                        <option>Brak (koniec wątku)</option>
                    </select>
                    <?php  ?>
                    <input type="text" name="continue-name" id="continue-name" placeholder="Imię postaci" disabled>
                <label><h4>Treść posta</h4></label>
                <div id="edit" contenteditable="true"></div>
                <div id="button-panel">
                    <button type="submit" id="submit-button" name="submit-button">Opublikuj</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="https://kit.fontawesome.com/324dc0f5f9.js" crossorigin="anonymous"></script>
    <script src="/forumPBP/media/js/text-editor.js"></script>



 