<?php
    session_start();
    require_once('../../logic/connect.php');

    if(isset($_SESSION['id']))
    {
        header("Location: /forumPBP/index.php");
        exit();
    }

    $login = $_POST['login'];
    $password = $_POST['password'];

    $_SESSION['form_login'] = $login;
    $_SESSION['form_pswd'] = $password;

    //sanityzacja danych przesłanych przez użytkownika
    $login = htmlentities($login, ENT_QUOTES, "UTF-8");

    //veryfikacja danych przesłanych przez użytkownika i wykonanie zapytania
    if ($result = $connect->query(sprintf("SELECT * FROM users WHERE userName='%s'",
    mysqli_real_escape_string($connect, $login))))
    {
        if($rows_count = $result->num_rows > 0)
        {
            $row = $result->fetch_assoc();                                  //przekaż wsztstkie elementy z wiersza
            if(password_verify($password, $row['userPassword']))
            {
                $last_visit_date = date("Y-m-d H:i:s");
                $query = "UPDATE users
                    SET userLastVisitDate = '$last_visit_date'
                    WHERE userName='$login'";
                    $res = @$connect->query($query);
                $_SESSION['id'] = $row['userID'];
                $_SESSION['user_name'] = $row['userName'];
                $_SESSION['user_avatar'] = $row['userAvatar'];
                $_SESSION['user_permission'] = $row['userPermission'];

                $result->free_result();                                     //pozbywa się z pamięci obiektu zapytania 

                header("Location: /forumPBP/index.php");                    //przeniesienie do strony głównej po zalogowaniu
            }
            else
            {
                $_SESSION['error'] .= 'Podano nieprawidłowe hasło<br>';
                header("Location: /forumPBP/index.php?page=login");
            }
            $connect->close();
            exit();
        }
        else
        {
            $_SESSION['error'] .= 'Podano nieprawidłowy login<br>';
            header("Location: /forumPBP/index.php?page=login");
            $connect->close();
            exit();
        }                                              
        $_SESSION['error'] = 'Błąd logowania';
        header("Location: /forumPBP/index.php?page=login");
        $connect->close(); 
        exit();
    }
?>