<?php
    session_start();
    require_once('../../logic/connect.php');

    $catTitle = $_POST['cat-name'];
    $catAuthor = $_SESSION['user_name'];
    $catPriority = $_POST['cat-type'];
    if (isset($_POST['cat-perm'])) $catPermission = $_POST['cat-perm'];
    else $catPermission = "admin";
    $currCatID = $_POST['curr-cat-id'];
    $siteID = $currCatID;


    if(isset($_POST['superior-cat']))
    {
        if ($currCatID = $_POST['superior-cat']) $superiorCat = $currCatID;
        else
        {
            $superiorCat = $_POST['superior-cat'];
            $query = "SELECT catID FROM categories WHERE catTitle= '$superiorCat'";
            $result = $connect->query($query);
            $answer = $result->fetch_assoc();
            $superiorCat = $answer['catID'];
        }

    }
    else if(isset($_POST['superior-subcat']))
    {
        $superiorCat = $_POST['superior-subcat'];
        $query = "SELECT catID FROM categories WHERE catTitle= '$superiorCat'";
        $result = $connect->query($query);
        $answer = $result->fetch_assoc();
        $superiorCat = $answer['catID'];
    }
    else
    {
        if ($currCatID == "0") $superiorCat = "0";
        else 
        {
            $query = "SELECT superiorCat FROM cathierarchylist WHERE inferiorCat = '$currCatID'";
            $result = $connect->query($query);
            $answer = $result->fetch_assoc();
            $superiorCat = $answer['superiorCat'];
        }
    }

    if ($currCatID == "0")
    {
        if ($catPriority == "Najwyższa") $catPriority = 0;
        else if ($catPriority == "Średnia") $catPriority = 1;
        else $catPriority = 2;
    }
    else
    {
        if (!is_numeric($currCatID)) $query = "SELECT catPriority FROM categories WHERE catTitle = '$currCatID'";
        else $query = "SELECT catPriority FROM categories WHERE catID = '$currCatID'";
        $result = $connect->query($query);
        $answer = $result->fetch_assoc();
        if ($catPriority == "Najwyższa") $catPriority = $answer['catPriority'];
        else if ($catPriority == "Średnia") echo $catPriority = $answer['catPriority']+1;
        else $catPriority = $answer['catPriority']+2;
    }

    $query = "INSERT INTO categories (catTitle, catAuthor, catPriority, catPermission)
        VALUES ('$catTitle','$catAuthor','$catPriority ','$catPermission')";
    $result = $connect->query($query);
    
    if ($superiorCat != "0")
    {
        $query = "SELECT catID FROM categories WHERE catTitle='$catTitle'";
        $result = $connect->query($query);
        $answer = $result->fetch_assoc();
        $newCatID = $answer['catID'];

        if(!is_numeric($superiorCat))
        {
            $query = "SELECT catID FROM categories WHERE catTitle= '$superiorCat'";
            $result = $connect->query($query);
            $answer = $result->fetch_assoc();
            $superiorCat = $answer['catID'];
        }

        $query = "INSERT INTO cathierarchylist (superiorCat, inferiorCat) VALUES ('$superiorCat','$newCatID')";
        $result = $connect->query($query);
    }
    
    $connect->close(); 

    if ($siteID != "0") header("Location:/forumPBP/index.php?page=home&subcat=".$siteID."&site=0");
    else header("Location:/forumPBP/index.php?page=home");
?>