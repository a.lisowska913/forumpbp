<?php
    session_start();
    $login = $_POST['login'];
    $email = $_POST['email'];
    $password1 = $_POST['password1'];
    $password2 = $_POST['password2'];
    $isFormOk = true;
    $_SESSION['error'] = "";


    if (strlen($login) < 4 || strlen($login) > 20)
    {
        $_SESSION['error'] .= "Login powinien mieć od 4 do 20 znaków.</br>";
        $isFormOk = false;
    }

    if(ctype_alnum($login) == false)
    {
        $_SESSION['error'] .= "Login może składać się tylko z liter i cyfr.</br>";
        $isFormOk = false;
    }

    $emailClean = filter_var($email, FILTER_SANITIZE_EMAIL);
    if((filter_var($emailClean, FILTER_VALIDATE_EMAIL)==false) || $emailClean != $email)
    {
        $_SESSION['error'] .= "Niepoprawny e-mail.</br>";
        $isFormOk = false;
    }

    if(strlen($password1) < 8 || strlen($password1) > 20)
    {
        $_SESSION['error'] .= "Hasło powinno mieć od 8 do 20 znaków.</br>";
        $isFormOk = false;
    }

    if($password1 != $password2)
    {
        $_SESSION['error'] .= "Hasła nie są identyczne.</br>";
        $isFormOk = false;
    }

    $password_hash = password_hash($password1, PASSWORD_DEFAULT);

    //zapamiętanie wprowadzonych w formularzu danych
    $_SESSION['form_login'] = $login;
    $_SESSION['form_email'] = $email;
    $_SESSION['form_pswd1'] = $password1;
    $_SESSION['form_pswd2'] = $password2;
    $_SESSION['form_rules'] = true;


    //operacje na bazie danych
    require_once('../../logic/connect.php');
    if ($result = $connect->query("SELECT userID FROM users WHERE userEmail='$email'"))
    {
        $emailCount = $result->num_rows;
        if($emailCount > 0)
        {
            $_SESSION['error'] .= "Email zajęty.</br>";
            $isFormOk = false;
        }
        $result->free_result();
    }

    if ($result = $connect->query("SELECT userID FROM users WHERE userName='$login'"))
    {
        $loginCount = $result->num_rows;
        if($loginCount > 0)
        {
            $_SESSION['error'] .= "Login zajęty.</br>";
            $isFormOk = false;
        }
        $result->free_result();
    }

    if ($isFormOk == false)
    {
        header("Location: /forumPBP/index.php?page=register");
    }
    else 
    {
        if($connect->query("INSERT INTO users (userName, userPassword, userEmail) VALUES ('$login', '$password_hash', '$email')"))
        {
            $_SESSION['error'] = "Zarejestrowano pomyślnie. Zaloguj się.";
            $_SESSION['form_login'] = $login;
            $_SESSION['form_pswd'] = $password1;
            header("Location: /forumPBP/index.php?page=login");
        }
        else
        {
            $_SESSION['error'] = "Błąd połączenia.</br>";
            header("Location: /forumPBP/index.php?page=register");
        }

    }
    $connect->close();
?>