<div class="main-container">
    <h1>Regulamin</h1>
    <blockquote>
        Nieznajomość regulaminu nie zwalnia od jego przestrzegania.
    </blockquote>
    <?php echo $separator; ?>
    <h4>Ogólne</h4>
    <ol>
        <li>Dołączając do forum akceptujesz rzeczy, zawarte w regulaminie. A nieznajomość regulaminu nie zwalnia od jego przestrzegania.</li>
        <li>Obowiązują podstawowe zasady kultury osobistej. Nie należy obrażać innych osób na czacie, niezależnie czy na tle płciowym, rasowym, wyznaniowym, koloru oczu, rodziny czy dowolnym innym.</li>
        <li>Przekleństwa nie są zakazane, ale nie należy ich nadużywać. Nie należy ich używać, określając innych użytkowników serwera.</li>
        <li>Nie należy publikować treści 18+ na kanałach ogólnych ani w wątkach, poza przeznaczonymi do tego kanałami (szczegóły niżej).</li>
        <li>Należy korzystać z kanałów zgodnie z ich przeznaczeniem.</li>
        <li>Zakazane jest reklamowanie swojego serwera discord, forum PBF, bloga PBP i tym podobnych bez zgody administracji.</li>
        <li>Umieszczanie grafik 18+ lub fekaliów jest surowo zakazane i z miejsca będzie karane warnem.
        Nie ma możliwości bycia obserwatorem na forum. Każdy wchodzący musi się zarejestrować, chcąc grać.</li>
    </ol>    
    <h4>Wątki</h4>
    <ol>
        <li>Długość odpisów powinna wynosić nie mniej, niż połowa tekstu napisanego przez drugiego piszącego. Jeśli nie pasuje ci długość odpisów osoby, z którą piszesz, rekomendowane jest zakończenie wątku.</li>
        <li>Respektujemy wykreowaną przez drugą osobę fikcję, nie wpływamy bezpośrednio na wybory jej postaci, nie zmieniamy tego, co zostało już zadeklarowane. Należy też respektować ogólne realia serwera.</li>
        <li>Dozwolona jest kreacja nowych frakcji, lokacji i postaci z uwzględnieniem już istniejących zasad serwera.</li>
        <li>Wątki ERP można prowadzić w prywatnych lokacjach, takich jak domy postaci, pokoje w karczmach, loża prywatna w karczmach lub w burdelu.</li>
        <li>W razie konfliktu z innym graczem należy go rozstrzygnąć przy użyciu mechaniki. Można również poprosić administrację o użycie mechaniki do rozstrzygnięcia konfliktu i interpretację wyników.</li>
        <li>Należy stosować się do zasad ortografii i interpunkcji. Starać się popełniać jak najmniej błędów, by wszystkim grającym przyjemnie czytało się wypowiedzi postaci.</li>
        <li>Po tygodniu nieaktywności drugiej osoby w wątku można go zakończyć bez pytania jej o zdanie.</li>
        <li>Postacie mogą prowadzić jednocześnie nieograniczoną ilość wątków. Należy jedynie kontrolować we własnym zakresie, w jakim punkcie chronologii znajduje się postać.</li>
    </ol>
    <h4>Odpisy w wątku</h4>
    <ol>
        <li>Dozwolone jest pisanie stylem książkowym, jednak dopuszcza się poza tym dowolne formatowanie tekstu cursywą, pogrubieniami czy podkreśleniami, jeśli ktoś preferuje ich używać.</li>
        <li>Odpisy powinno się wykonywać przy pomocy postaci, która pisze. Na koniec należy oznaczyć osobę, która ma kontynuować nasz wątek lub oznaczyć możliwość dołączenia dowolnej osoby do wątku.</li>
        <li>Można umieszczać komentarze w odpisie, na samym jego końcu w: <b>( )</b> lub <b>[ ]</b></li>
        <li>Po zakończeniu wątku należy oznaczyć wątek jako zakończony, jednocześnie zwalniając go dla innych graczy.</li>
    </ol>
    <h4>Kary</h4>
    <ol>
        <li>Za rażące i nagminne łamanie regulaminu można otrzymać upomnienie w postaci @warn. Bez konsekwencji można otrzymać dwa upomnienia. Trzecie upomnienie automatycznie skutkuje banem (na czas określony lub stałe)</li>
        <li>Osoby z banem nie są w stanie pisać w żadnych wątkach a ich postacie zostają zawieszone.
        <li>Upomnienie zostaje usunięte po 2 tygodniach bez otrzymania kolejnego.</li>
    </ol>
    <h4>Mistrzowie Gry</h4>
    <ol>
        <li>Mistrz Gry (MG) jest osobą, która czasem prowadzi innym graczom zlecenia.</li>
        <li>MG może zostać każdy gracz na serwerze, posiadający przynajmniej 1 zweryfikowaną pozytywnie postać oraz co najmniej jeden wątek.</li>
        <li>Administracja może odmówić osobie zastania MG, jeśli dana osoba łamała wcześniej regulamin.</li>
        <li>MG zostaje się poprzez uzgodnienie z administracją wątku "zlecenia" i dodanie go do aktywnych zleceń. Przy czym pierwsze zlecenie prowadzone przez MG będzie zleceniem testowym, po którym administrator może odebrać MG rolę.</li>
        <li>Podczas prowadzenia MG zobowiązuje się do nienadużywania swojej władzy narratora, zabronione jest poniżanie graczy i celowe zawalanie wszystkich akcji, których się podejmą.</li>
        <li>Spory pomiędzy graczem a MG powinny być rozwiązywane przy pomocy mechaniki. Poprzez spory rozumie się zarówno walkę, jak i inne wydarzenia, których wynik w mniejszym lub większym stopniu zależał od szczęścia lub różnicy w umiejętnościach pomiędzy graczem a jego przeciwnikiem. Użycie kości każdorazowo powinno coś zmieniać - unikamy pustych rzutów. Jeśli rzut był dobry dzieje się coś pozytywnego, jeśli zły - coś negatywnego. Jeśli w danej sytuacji nie mogłoby się zdarzyć zarówno coś pozytywnego, jak i coś negatywnego, nie należy rzucać. Tak samo, jeśli negatywny wynik zablokuje wszystkie możliwe ścieżki dalszego rozwoju fabuły.</li>
        <li>MG może dotkliwie ranić postacie, nie może jednak ich zabić (przegrana postaci oznacz, że zostaje wyeliminowana z wątku np. z powodu dotkliwych ran), chyba że gracz zgodzi się na śmierć.</li>
        <li>MG powinien być przede wszystkim fanem swoich graczy - stawiać przed nimi wyzwania, żeby zmusić ich do kombinowania i wyciągać konsekwencje z ich działań, jednak całość mimo wszystko powinno rozgrywać się na korzyść graczy lub w sposób dla graczy niekrzywdzący.</li>
        <li>Tak samo jak GM zobowiązany jest do respektowania graczy, tak gracze zobowiązani są do respektowania GMa oraz wkładanej przez niego pracy. </li>
    </ol>

    <?php echo $separator; ?>
</div>