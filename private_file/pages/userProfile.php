<?php
    $query = "SELECT * FROM users WHERE userName = '$userName'";
    $result = $connect->query($query);
    $answer = $result->fetch_assoc();
    $membership = $answer['userMembership'];
    $joinDate = $answer['userJoinDate'];
    $lastVisitDate = $answer['userLastVisitDate'];
    $characterCount = $answer['userCharacterCount'];
    $postCount = $answer['userPostCount'];
    $quote = $answer['userQuote'];
    $result->free();
    $connect->close();
?>

<div class="main-container">
    <?php echo $separator; ?>
    <div class="user-containter">
        <div class="user-card">
            <div class="user-avatar-panel">
                <img class="user-avatar-image" src="
                <?php echo $avatar;?>
                ">
            </div>
            <div class="user-data-panel">
                <h2 style="align-left"><?php echo $userName; ?></h1>
                <b>Staż:</b> <?php echo $membership; ?><br>
                <b>Data dołączenia:</b> <?php echo $joinDate; ?><br>
                <b>Data ostatniej wizyty:</b> <?php echo $lastVisitDate; ?><br>
                <b>Liczba postaci:</b> <?php echo $characterCount; ?><br>
                <b>Liczba postów:</b> <?php echo $postCount; ?><br><br>
            </div>
        </div>
        <div class="char-list-panel">
            <table>
                <tr class='subcategory'>
                    <td style="max-width: 10px">No.</td><td>Imię postaci</td><td>Ilość postów</td>
                </tr>
                <?php for($i=0;$i<10;$i++)
                {
                    echo "
                    <tr class = 'char-record-light'>
                        <td style='max-width: 10px'>0</td><td><a href=''>Aria</a></td><td>0</td>
                    </tr>";
                    echo "
                    <tr class = 'char-record-dark'>
                        <td style='max-width: 10px'>0</td><td><a href=''>Aria</a></td><td>0</td>
                    </tr>";
                }
                ?>
                    
            </table>
        </div>
        <div class="user-quotation-panel">
            <i class="profile-quote"><?php echo $quote; ?></i>
        </div>
        <a href="">
        <div class="create-character-panel">
            Stwórz postać
        </div>
        </a>
    </div>

    <div class="user-operations-panel">
        <div class='operation-button'>Aktywne&nbspwątki</div>
        <div class='operation-button'>Oczekujące&nbspwątki</div>
        <div class='operation-button'>Poszkujący</div>
    </div>
    <br>
    <br>
    <?php echo $separator; ?>

</div>