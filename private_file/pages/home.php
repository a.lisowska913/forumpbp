<?php
    require_once('private_file/logic/connect.php');

    if(isset($_GET['subcat'])) 
    {
        $catID = $_GET['subcat'];
        require_once('private_file/logic/auth-cat.php');
    }

    echo "<div class='base-nav'>";
    if(isset($_GET['subcat']))
    {
        $query = "SELECT superiorCat FROM cathierarchylist WHERE inferiorCat='$catID'";
        $result = $connect->query($query);
        $answer = $result->fetch_assoc();
        $superiorCat = $answer['superiorCat'];

        $catID = $_GET['subcat'];
        $query = "SELECT superiorCat FROM cathierarchylist WHERE inferiorCat='$superiorCat'";
        $result = $connect->query($query);
        $answer = $result->fetch_assoc();
        $superiorCat = $answer['superiorCat'];
    

        $query = "SELECT catTitle, catPriority FROM categories WHERE catID='$superiorCat'";
        $result = $connect->query($query);
        $answer = $result->fetch_assoc();
        $superiorCatTitle = $answer['catTitle'];
        $superiorCatPriority = $answer['catPriority'];

        if ($superiorCatPriority<1)
        {
            echo "
                <div class='base-nav-button'>
                    <a href='/forumPBP/index.php?page=home'>Powrót&nbspdo&nbspStrony&nbspGłównej</a>
                </div>";
                if ($userPermission == "mod" || $userPermission == "admin")
                {
                    echo
                    "<div class='base-nav-button'>
                        <a href='javascript:void(0);' id='insert-button' onclick='mySecondFunction()'>
                            Wstaw&nbspkategorię
                        </a>
                        <script src='".$media_path."/js/adding-expandable.js'></script>    
                    </div>";
                }       
        }
        else
        {
            echo "
                <div class='base-nav-button'>
                    <a href='/forumPBP/index.php?page=home&subcat=".$superiorCat."'>Powrót&nbspdo&nbsp".$superiorCatTitle."</a>
                </div>";
            if ($userPermission == "mod" || $userPermission == "admin")
            {
                echo
                "<div class='base-nav-button'>
                    <a href='javascript:void(0);' id='insert-button' onclick='mySecondFunction()'>
                        Wstaw&nbspwątek
                    </a>
                    <script src='".$media_path."/js/adding-expandable.js'></script>    
                </div>";
            }
        }
    }
    else if ($userPermission == "admin")
        {
            echo
            "<div class='base-nav-button'>
                <a href='javascript:void(0);' id='insert-button' onclick='mySecondFunction()'>
                    Wstaw&nbspkategorię
                </a>
                <script src='".$media_path."/js/adding-expandable.js'></script>    
            </div>";
        }
    echo "</div>";
    include("cat-editor.php");
?>

<table>
    <?php
        

        if(isset($_GET['subcat']))
        {
            $query = "SELECT catTitle, catPriority, catID FROM categories WHERE catID='$catID'";
        }
        else $query = "SELECT * FROM categories WHERE catPriority='0'";
        $result0 = $connect->query($query);

        while($rowCat = $result0->fetch_assoc())
        {
            $superiorCat = $rowCat['catID'];
            
            echo "<tr class='category'>
            <td colspan = '2' class='adjust-left'>
            ".$rowCat['catTitle'];
            if ($rowCat['catPriority'] < 3)
            {
                echo "</td>";
                if ($userPermission == "admin")
                echo "<td><form action='/forumPBP/private_file/pages/action/deletecatAction.php' method=post>
                <input type='hidden' name=element-to-delete-title value=".$rowCat['catID'].">
                <input type='submit' class='delete-button' value='usuń'></td>";
                echo "<td>Wątki</td> <td>Posty</td> <td>Ostatni post</td> </tr>";
            }
            else
            {
                echo "</td>";
                if ($userPermission == "admin")
                echo "<td><form action='/forumPBP/private_file/pages/action/deletecatAction.php' method=post>
                <input type='hidden' name=element-to-delete-title value=".$rowCat['catID'].">
                <input type='submit' class='delete-button' value='usuń'></td>";
                echo "<td>Autor</td> <td>Posty</td> <td>Wyświetlenia</td> <td>Ostatni post</td>";
            }

            $query = "SELECT * FROM cathierarchylist WHERE superiorCat='$superiorCat'";
            $result1 = $connect->query($query);
            while($rowSubcat = $result1->fetch_assoc())
            {
                $inferiorCat = $rowSubcat['inferiorCat'];
                $query = "SELECT * FROM categories WHERE catID='$inferiorCat'";
                if ($result2 = $connect->query($query))
                {
                    $currRow = $result2->fetch_assoc();
                    if ($currRow['catPriority'] != 5)
                    {
                        echo "<tr class='subcategory'>
                            <td colspan='2' class='adjust-left'>".$currRow['catTitle']."</td>";
                        echo "<td><form action='/forumPBP/private_file/pages/action/deletecatAction.php' method=post>
                        <input type='hidden' name=element-to-delete-title value=".$inferiorCat.">
                        <input type='submit' class='delete-button' value='usuń'></td>";
                        echo "<td colspan='3'></td>
                        </tr>";
                    }
                    else
                    {
                        $permissionToInferiorSubcat = $currRow['catPermission'];
                        if ($permissionToInferiorSubcat == "user") $permissionToInferiorSubcat = "każdego";
                        echo "<tr class = 'record'>
                            <td rowspan = '2' class='icon-cell'><img class='icon-plot-status' src ='".$images_path."/noNewMessageIcon.png'></td>
                            <td class='adjust-left'><b>
                            <a href='/forumPBP/index.php?page=plot&plotid=".$currRow['catID']."&site=0'>"
                            .$currRow['catTitle']."</a></b></td>";
                            if ($userPermission == "admin")
                            echo "<td  rowspan='2' class='record-bot' style='text-align:center'>
                            <form action='/forumPBP/private_file/pages/action/deletecatAction.php' method=post>
                            <input type='hidden' name=element-to-delete-title value=".$inferiorCat.">
                            <input type='submit' class='delete-button' value='usuń'></td>";
                            echo "<td rowspan='2' class='record-bot'>".$currRow['catAuthor']."</td>
                            <td rowspan='2' class='record-bot'>".$currRow['catPlotCount']."</td>
                            <td rowspan ='2' class='record-bot'>".$currRow['catPostCount']."</td>
                            <td>".$currRow['catLastPostDate']."</td>
                        </tr>
                        <tr class = 'record'>
                            <td class='adjust-left-record-bot'>dostępne dla: ".$permissionToInferiorSubcat."</td>
                            <td class='record-bot'><b>Autor: </b>".$currRow['catLastPostAuthor']."</td>
                        </tr>";
                    }
                }

                $query = "SELECT * FROM cathierarchylist WHERE superiorCat='$inferiorCat'";
                $result2 = $connect->query($query);
                while($rowInferiorCat = $result2->fetch_assoc())
                {
                    $inferiorSubcat = $rowInferiorCat['inferiorCat'];
                    $query = "SELECT * FROM categories WHERE catID='$inferiorSubcat'";
                    $result3 = $connect->query($query);
                    $currRow = $result3->fetch_assoc();
                
                    $permissionToInferiorSubcat = $currRow['catPermission'];
                    if ($permissionToInferiorSubcat == "user") $permissionToInferiorSubcat = "każdego";
                    echo "<tr class = 'record'>
                        <td rowspan = '2' class='icon-cell'><img class='icon-plot-status' src ='".$images_path."/noNewMessageIcon.png'></td>
                        <td class='adjust-left'><b>
                        <a href='/forumPBP/index.php?page=home&subcat=".$currRow['catID']."&site=0'>"
                        .$currRow['catTitle']."</a></b></td>";
                        if ($userPermission == "admin")
                        echo "<td rowspan='2' class='record-bot' style='text-align:center'><form action='/forumPBP/private_file/pages/action/deletecatAction.php' method=post>
                        <input type='hidden' name=element-to-delete-title value=".$inferiorSubcat.">
                        <input type='submit' class='delete-button' value='usuń'></td>";
                        echo "<td rowspan='2' class='record-bot'>".$currRow['catPlotCount']."</td>
                        <td rowspan ='2' class='record-bot'>".$currRow['catPostCount']."</td>
                        <td>".$currRow['catLastPostDate']."</td>
                    </tr>
                    <tr class = 'record'>
                        <td class='adjust-left-record-bot'>dostępne dla: ".$permissionToInferiorSubcat."</td>
                        <td class='record-bot'><b>Autor: </b>".$currRow['catLastPostAuthor']."</td>
                    </tr>";
                }
            }
        } 

        $result0->free_result(); 
        if(isset($result1)) $result1->free_result();
        if(isset($result2)) $result2->free_result();
        if(isset($result3)) $result3->free_result();
        $connect->close();
    ?>
</table>

<div id="fast-actions-container">
    <a class="setReadAll" href="">Oznacz wszystko jako przeczytane</a>
</div>