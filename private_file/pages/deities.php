<div class="main-container">
    <h1>Bóstwa</h1>
    <blockquote>Panteon Dziesiątki bytów, w które mieszkańcy Airth powszechnie wierzą. Ich kulty mogą przyjmować różne formy, a bóstwa mogą mieć różny wygląd i imiona, jednak zawsze będzie to ta sama dziesiątka.</br>
    Praktycznie każdy mieszkaniec Airth wierzy w któreś bóstwo, lub któryś zestaw bóstw. Nawet jeśli nie jest bardzo pobożny, albo wręcz niewierzący, rytuały, takie jak modlitwa do Fuliosa przed podróżą, czy składanie ofiar Serafonowi, by plony były dobre, są częstymi praktykami wśród wszystkich grup społecznych.</br>
    Kapłani poszczególnych bóstw są w stanie prosić swoich patronów o przysługi z ich zakresu. Czasem nawet otrzymują odpowiedź. Najczęściej jednak możliwość korzystania z boskiej mocy jest raczej predyspozycjami magicznymi krwi danego osobnika do magii podobnej magii bóstwa, niż rzeczywistym wpływem z zewnątrz. Niemniej wytknięcie kapłanowi faktu, że jest praktycznie magiem jest niesłychanym nietaktem i można zostać za to wyklętym przez wszystkie kulty.</blockquote>
    <?php echo $separator; ?>

    <h2>Spis bóstw:</h2>

    <h4>Serafon</h4>
    <i>Bóg płodności i dobrobytu</i>
    </br></br>
    <h4>Octavia</h4>
    <i>Bogini przestępców</i>
    </br></br>
    <h4>Aurora</h4>
    <i>Bogini ukojenia i snu</i>
    </br></br>
    <h4>Uros</h4>
    <i>Bóg handlu i rzemiosła</i>
    </br></br>
    <h4>Laurette</h4>
    <i>Bogini magii i żeglarstwa</i>
    </br></br>
    <h4>Vedette</h4>
    <i>Bóg prawa i porządku</i>
    </br></br>
    <h4>Vigilus</h4>
    <i>Bóg wojny i śmierci</i>
    </br></br>
    <h4>Alivia</h4>
    <i>Bogini medycyny i informacji</i>
    </br></br>
    <h4>Synnadil</h4>
    <i>Bogini arystokracji i przyjemności</i>
    </br></br>
    <h4>Fulios</h4>
    <i>Bóg wolności</i>
    </br></br>
    <?php echo $separator; ?>
</div>