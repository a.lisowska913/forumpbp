<div class="form">
    <h1>Rejestracja</h1>
    <form action="/forumPBP/private_file/pages/action/registerAction.php" name="registration-form" method="post">
        <input type="text"
        value="<?php if(isset($_SESSION['form_login']))
            {
                echo $_SESSION['form_login'];
                unset($_SESSION['form_login']);
            }?>"
        name="login" placeholder="Login" required /></br>
        <input type="email"
        value="<?php if(isset($_SESSION['form_email']))
            {
                echo $_SESSION['form_email'];
                unset($_SESSION['form_email']);
            }?>"
        name="email" placeholder="E-mail" required /></br>
        <input type="password"
        value="<?php if(isset($_SESSION['form_pswd1']))
            {
                echo $_SESSION['form_pswd1'];
                unset($_SESSION['form_pswd1']);
            }?>"
        name="password1" placeholder="Hasło" required /></br>
        <input type="password"
        value="<?php if(isset($_SESSION['form_pswd2']))
            {
                echo $_SESSION['form_pswd2'];
                unset($_SESSION['form_pswd2']);
            }?>"
        name="password2" placeholder="Powtórz hasło" required /></br>
        <label>
            <input type="checkbox" name="rules"
            <?php
                if(isset($_SESSION['form_rules'])) echo "checked"
            ?>
            required />
            Akceptuję <a href="/forumPBP/index.php?page=rules">regulamin</a>
        </label></br>
        <input type="submit" name="submit" value="Zarejestruj" />
    </form>
</div>

<p class="login-error">
    <?php if (isset($_SESSION['error']))
    {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }?>
</p>