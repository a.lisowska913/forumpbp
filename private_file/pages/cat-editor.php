<?php 
    require_once('private_file/logic/connect.php');


    if(isset($_GET['subcat'])) $currCatID = $_GET['subcat'];
    else $currCatID = "0";

    if(!isset($_GET['subcat']) || ((isset($_GET['subcat']) && $superiorCatPriority<1) ))
    {
        echo "

<div class='text-editor-container' id='cat-editor'>   
    <div class='text-editor-form'>
        <form method='post' action='/forumPBP/private_file/pages/action/cateditorAction.php'>
            <input type=hidden name='curr-cat-id' value=".$currCatID.">
            <label><h4>Typ kategorii</h4></label>
                <select id='cat-type' name='cat-type'>
                    <option selected>Najwyższa</option>
                    <option>Średnia</option>
                    <option>Najniższa</option>
                </select><br />

                <select id='superior-cat' name='superior-cat' disabled>";
                    if ($currCatID == "0")
                    {
                        $query = "SELECT * FROM categories WHERE catPriority = '0'";
                        $result = $connect->query($query);
                        while($answer = $result->fetch_assoc())
                        {
                            echo "<option selected>".$answer['catTitle']."</option>";
                        } 
                    }
                    else
                    {
                        $query = "SELECT catTitle FROM categories WHERE catID = '$currCatID'";
                        $result = $connect->query($query);
                        $answer = $result->fetch_assoc();
                        echo "<option selected>".$answer['catTitle']."</option>";
                    }
                echo "
                </select><br />

                <select id='superior-subcat' name='superior-subcat' disabled>
                ";

                if ($currCatID == "0")
                {
                    $query = "SELECT * FROM categories WHERE catPriority = '1'";
                    $result = $connect->query($query);
                    while($answer = $result->fetch_assoc())
                    {
                        echo "<option selected>".$answer['catTitle']."</option>";
                    } 
                }
                else
                {
                    $query = "SELECT categories.catTitle, categories.catPriority, categories.catID, cathierarchylist.superiorCat
                    FROM categories
                    INNER JOIN cathierarchylist ON categories.catID=cathierarchylist.inferiorCat";
                    $result = $connect->query($query);
                    while($answer = $result->fetch_assoc())
                    {
                        if ($answer['superiorCat'] == $currCatID && $answer['catPriority'] == "3")
                            echo "<option selected>".$answer['catTitle']."</option>";
                    }
                }
                
                echo "
                </select>
                
            <label><h4>Nazwa kategorii</h4></label>
                <input type='text' name='cat-name' id='cat-name' placeholder='Nazwa kategorii' required>
           
            <label><h4>Uprawnienia kategorii</h4></label>
                <select id='cat-perm' name='cat-perm' disabled>
                    <option selected>admin</option>
                    <option>mod</option>
                    <option>mg</option>
                    <option>user</option>
                </select>
                <br />
                <button type='submit' id='submit-button' name='submit-button'>Dodaj</button>
        </form>
    </div>
</div>
";
    }
    else
    {
        echo "
<div class='text-editor-container' id='cat-editor'>   
    <div class='text-editor-form'>
        <form method='post' action='/forumPBP/private_file/pages/action/cateditorAction.php'>               
            <label><h4>Nazwa wątku</h4></label>
                <input type='text' name='cat-name' id='cat-name' placeholder='Nazwa kategorii' required>
                <input type=hidden name='cat-type' value='Średnia'>
                <input type=hidden name='superior-cat' value=".$currCatID.">
                <input type=hidden name='curr-cat-id' value=".$currCatID.">
           
            <label><h4>Uprawnienia wątku</h4></label>
                <select id='plot-perm' name='plot-perm'>
                    <option selected>admin</option>
                    <option>mod</option>
                    <option>mg</option>
                    <option>user</option>
                </select>
                <br />
                <button type='submit' id='submit-button' name='submit-button'>Dodaj</button>
        </form>
    </div>
</div>
        ";
    }
?>
<script src="https://kit.fontawesome.com/324dc0f5f9.js" crossorigin="anonymous"></script>
    <script src="/forumPBP/media/js/cat-editor.js"></script>



 