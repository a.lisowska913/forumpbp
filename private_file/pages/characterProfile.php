<?php
    $query = "SELECT * FROM charactersheet WHERE characterName = 'Argona'";
    $result = $connect->query($query);
    $answer = $result->fetch_assoc();
    $name = "Argona";
    $characterAvatar = $answer['characterAvatar'];
    $race = $answer['characterRace'];
    $dateBirth = $answer['characterDateBirth'];
    $gender = $answer['characterGender'];
    $height = $answer['characterHeight'];
    $weight = $answer['characterWeight'];
    $origin = $answer['characterOrigin'];
    $socialStatus = $answer['characterSocialStatus'];
    $job = $answer['characterJob'];
    $personality = $answer['characterPersonality'];
    $story = $answer['characterStory'];
    $endurance = $answer['characterEndurance'];
    $strength = $answer['characterStrength'];
    $agility = $answer['characterAgility'];
    $initiative = $answer['characterInitiative'];
    $will = $answer['characterWill'];
    $charisma = $answer['characterCharisma'];
    $meele = $answer['characterMeele'];
    $ranged = $answer['characterRanged'];
    $intelligence = $answer['characterIntelligence'];

    $domain = "none";
    $power = "none";
?>

<div class="main-container">
    <?php echo $separator; ?>
    <div class="char-containter">
        <div class="char-card">
            <div class="char-avatar-panel">
                <img class="char-avatar-image" src="<?php echo $characterAvatar;?>">
            </div>
            <div class="char-data-panel">
                <h2 id="character-name"><?php echo $name; ?></h2>
                <b>Rasa:</b> <?php echo $race; ?><br />
                <b>Płeć:</b> <?php echo $gender; ?><br />
                <b>Wzrost i waga:</b> <?php echo $height."cm i ".$weight."kg"; ?><br />
                <b>Pochodzenie:</b> <?php echo $origin; ?><br />
                <b>Status społeczny:</b> <?php echo $socialStatus; ?><br />
                <b>Praca:</b> <?php echo $job; ?><br />
                <b>Historia:</b> <?php echo $story; ?><br />
            </div>
        </div>
        <div class="char-stats-panel">
            <div class="char-data-panel">
                <b>Domeny:</b> <?php echo $domain; ?><br />
                <b>Moce:</b> <?php echo $power; ?><br />
            </div>
            <br />
            <h4 id="stats">Statystyki:</h4>
            <table id="stats-table">
                <tr class="subcategory">
                    <td>Wt</td> <td>S</td> <td>Zw</td> <td>I</td> <td>SW</td> <td>Cha</td>
                    <td>WW</td> <td>WD</td> <td>Int</td>
                </tr>
                <tr class="record">
                    <td><br /><?php echo $endurance; ?></td>
                    <td><br /><?php echo $strength; ?></td>
                    <td><br /><?php echo $agility; ?></td>
                    <td><br /><?php echo $initiative; ?></td>
                    <td><br /><?php echo $will; ?></td>
                    <td><br /><?php echo $charisma; ?></td>
                    <td><br /><?php echo $meele; ?></td>
                    <td><br /><?php echo $ranged; ?></td>
                    <td><br /><?php echo $intelligence; ?></td>
                </tr>
            </table>
        </div>
        <div class="panel">
        </div>
        <div class="panel">
        </div>
    </div>

    <div class="char-operations-panel">
        <div class='operation-button'>Aktywne&nbspwątki</div>
        <div class='operation-button'>Oczekujące&nbspwątki</div>
        <div class='operation-button'>Poszkujący</div>
    </div>
    <br>
    <br>
    <?php echo $separator; ?>

</div>