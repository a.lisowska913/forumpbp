<?php
    require_once('private_file/logic/connect.php');
    
    $plotID = $_GET['plotid'];
    $query = "SELECT catTitle FROM categories WHERE catID='$plotID'";
    $result = $connect->query($query);
    $answer = $result->fetch_assoc();
    $plotTitle = $answer['catTitle'];

    $query = "SELECT superiorCat FROM cathierarchylist WHERE inferiorCat='$plotID'";
    $result = $connect->query($query);
    $answer = $result->fetch_assoc();
    $superiorCat = $answer['superiorCat'];

    $query = "SELECT catTitle FROM categories WHERE catID='$superiorCat'";
    $result = $connect->query($query);
    $answer = $result->fetch_assoc();
    $superiorCatTitle = $answer['catTitle'];
    $superiorCatTitle = str_replace(' ', '&nbsp', $superiorCatTitle);

    if(isset($_GET['site'])) $site = $_GET['site'];
    else $site = 0;
?>

<div class="base-nav">
    <div class="base-nav-button">
        <?php
        echo "<a href='/forumPBP/index.php?subcat=".$superiorCat."'>Powrót&nbspdo&nbsp$superiorCatTitle</a>"
        ?>
    </div>
    <div class="base-nav-button">
        
    <a href="javascript:void(0);" id="insert-button" onclick="myFunction()">
        Wstaw&nbsppost
    </a>
    <script src="<?php echo $media_path;?>/js/adding-expandable.js"></script>
    </div>
</div>


<?php include("text-editor.php"); ?>

<table>
    <tr class="category">
        <td colspan = '5'><h2 style="color:var(--primaty-text)">"<?php echo $plotTitle; ?>"</h2></td>
    </tr>
    <tr class="subcategory"><td>Autor</td> <td colspan = '4'>Treść</td></tr>

    <?php        
        $query = "SELECT * FROM posts WHERE plotID='$plotID' ORDER BY postDate DESC";
        $result = $connect->query($query);

        $row_iterator = 0;
        while($row = $result->fetch_assoc())
        {
            
            if (($row_iterator-$row_iterator%5)/5 == $site)
            {
                $is_last_site = true;
                if(isset($row))
                {
                    $post_plot = $row['plotID'];
                    $post_author = $row['postAuthor'];
                    $post_title = $row['postTitle'];
                    $post_content = $row['postContents'];
                    $post_date = $row['postDate'];
                    $post_character_author;

                    $query = "SELECT * FROM users WHERE userName='$post_author'";
                    $result1 = $connect->query($query);
                    $answer = $result1->fetch_assoc();

                    $author_perm = $answer['userPermission'];
                    $author_avatar = $answer['userAvatar'];
                    $author_quote = $answer['userQuote'];
                    if ($author_avatar == "emptyAvatarIcon.png") $author_avatar = $images_path."/".$author_avatar;

                    echo
                    "<tr class = 'record'>
                        <td style='max-width:100px' id='user-data' rowspan = '4'>
                            <img class='user-avatar' src='".$author_avatar."'><br>
                            ".$post_author."<br>
                            ".$author_perm."
                        </td>
                        <td colspan= '4' class='adjust-left'><b>Wysłano dnia:</b> ".$post_date."</td>
                    </tr>
                    <tr class = 'record'>
                        <td colspan= '4' id='post-title'><h3>".$post_title."</h3></td>
                    </tr>
                    <tr class = 'record'>
                        <td id='post-inside' colspan= '4' style='text-align:justify'><p>".$post_content."</p></td>
                    </tr>
                    <tr class = 'record'>
                        <td id='post-quote' colspan= '4' style='text-align:justify'><p>".$author_quote."</p></td>
                    </tr>
                    <tr id='post-division'><td colspan = '5'></td></tr>";

                    $is_last_site = false;
                } 
            }
            $row_iterator++;
        }
        
    ?>
</table>

<?php
    $result->free_result(); 
    if(isset($result1)) $result1->free_result();
    $connect->close();

    $next_site_no = $site + 1;
    $prev_site_no = $site - 1;
    if ($site != 0) $previous_site = "index.php?page=plot&plotid=".$plotID."&site=".$prev_site_no."";
    if (isset($is_last_site) == true) $next_site = "index.php?page=plot&plotid=".$plotID."&site=".$next_site_no."";
?>

<div class="plot-footer">
    <?php
        if ($site != 0)
        {
            echo "<a href='".$previous_site."'><img id='site-nav' src='".$images_path."/arrowPrevious.png'></a>";
        }
        if (isset($is_last_site) == true)
        {
            echo "<a href='".$next_site."'><img id='site-nav' src='".$images_path."/arrowNext.png'></a>";
        }
    ?>
</div>