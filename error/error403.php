<?php
    include("../private_file/logic/setTheme.php");
    $media_path = "/forumPBP/media/";
    $images_path = "/forumPBP/".$images_path;
?>
<script src="<?php echo $media_path?>js/toggle-theme.js"></script>
<html>
    <head>
        <title>Airth :: Error 403</title>
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleUniwersal.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleMain.css">
        <link rel="stylesheet" href="<?php echo $media_path;?>css/styleGadgets.css">
    </head>
    <body>
        <main>
            <div class="main-container">
                <img id="frame-right" src ="<?php echo $images_path?>/frameRight.png">
                <img id="frame-left" src ="<?php echo $images_path?>/frameLeft.png">
                <a href="/forumPBP"><h1 class="error">Error&nbsp403</h1></a>   
            </div>
        </main>
    </body>
</html>