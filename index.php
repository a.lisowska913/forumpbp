<?php
    session_start();
    include("private_file/logic/configs.php");

    if (empty($_SESSION['user_permission']) || $_SESSION['user_permission'] == null)    //jeśli użytkownik nie jest zalogowany lub się wylogował
        $_SESSION['user_permission'] = "guest";                                         //ustaw dostęp jako "gość"
    (empty($_GET['page'])) ? $url = "home" : $url = $_GET['page'];                      //jeśli strona nie zostałą wybrania, przekieruj do "home"

    $file = "private_file/pages/".$url.".php";                                          //ścieżka dostępu do plików ze stronami
    $title = $page_name[$url];

    require_once("private_file/logic/auth.php");
    require_once("private_file/logic/auth-cat.php");

    if(file_exists($file)) 
    {
        $userPermission = $_SESSION['user_permission'];
        include("private_file/logic/setTheme.php");
        include("private_file/modules/header.php");
        include($file);
        include("private_file/modules/footer.php");
    }
    else
    {
        header("Location: error/error404.php");
        exit();
    }
?>