const textEditorButton = document.querySelectorAll('.te-button');

textEditorButton.forEach(textEditorButton => {
    textEditorButton.addEventListener('click', () => {
        let buttonCommand = textEditorButton.dataset['element'];

        if(buttonCommand == 'fontSize')
        {
            size = prompt('Podaj rozmiar: ', 'musi być od 0 do 7');
        }
        if (buttonCommand == 'insertImage' || buttonCommand == 'createLink')
        {
            let url = prompt('Podaj adres URL: ', 'http://');
            document.execCommand(buttonCommand, false, url);
        }
        else if (buttonCommand == 'formatBlock')
        {
            document.execCommand(buttonCommand, false, "");
        }
        else
        {
            document.execCommand(buttonCommand, false, null);
        }
    });
});

var edit = document.getElementById('edit');
edit.innerHTML = localStorage.getItem('edit');

edit.addEventListener('blur', function() {
    localStorage.setItem('edit', edit.innerHTML);
});

var submitButton = document.getElementById('submit-button');
submitButton.addEventListener('click', function() {
    localStorage.setItem('edit', edit.innerHTML);
    document.cookie = "content=" + localStorage.getItem('edit');
    localStorage.setItem('edit', ""); 
});

var whoContinueButton = document.getElementById("who-continue");
var whoContinueName = document.getElementById("continue-name");

whoContinueButton.addEventListener("click", function() {
    if (whoContinueButton.value == "Konkretna osoba") whoContinueName.disabled = false;
    else whoContinueName.disabled = true;
});