function myFunction() {
    var menu = document.getElementById("menu");
    var arrow = document.getElementById("arrowIcon1");
    if (menu.style.height == "515px") {
        menu.style.height = "0px";
        arrow.style.transform = "rotateZ(0deg)";
    } else {
        menu.style.height = "515px";
        arrow.style.transform = "rotateZ(180deg)";
    }
}
onresize = (event) => {
    menu.style.height = "0px";
    arrow.style.transform = "rotateZ(0deg)"
};