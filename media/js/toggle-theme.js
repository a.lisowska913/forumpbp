var toggleButton = document.getElementById("theme-toggle-button");

var currentStoredTheme = localStorage.getItem('theme');
if (currentStoredTheme) document.documentElement.setAttribute('current-theme', currentStoredTheme)
{
    toggleButton.onclick = function()
    {
        var currentTheme = document.documentElement.getAttribute("current-theme");
        var targetTheme;

        if (currentTheme === "light") targetTheme = "dark";
        else targetTheme = "light";
        
        document.documentElement.setAttribute('current-theme', targetTheme)
        localStorage.setItem('theme', targetTheme);

        document.cookie = "theme=" + targetTheme ;

        location.reload();
    };
}