var catType = document.getElementById("cat-type");
var superiorCat = document.getElementById("superior-cat");
var superiorSubcat = document.getElementById("superior-subcat");
var catPermissions = document.getElementById("cat-perm");

catType.addEventListener("click", function() {
    if (catType.value == "Średnia")
    {
        superiorCat.disabled = false;
    }
    else
    {
        superiorCat.disabled = true;
    }

    if (catType.value == "Najniższa")
    {
        superiorSubcat.disabled = false;
        catPermissions.disabled = false;
    }
    else
    {
        superiorSubcat.disabled = true;
        catPermissions.disabled = true;
    }
});


